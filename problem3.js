function problem3(inventory,sortCars)
{
   n=inventory.length;
   for( i=0; i<n; i++ )
   {
      for( j=i+1; j<n; j++ )
      {
         if( sortCars[i].car_model > sortCars[j].car_model )
         {
            t=sortCars[i];
            sortCars[i]=sortCars[j];
            sortCars[j]=t;
         }
      }
   }
   return sortCars;
}
module.exports=problem3;