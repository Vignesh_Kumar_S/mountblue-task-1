//Problem 6 : Create array that contains only BMW and Audi cars

const inventory = require("../inventory");
const problem6 = require("../problem6");

let BMWAudi=[];
let result = problem6(inventory,BMWAudi);

//Print by using JSON.stringify
result = JSON.stringify(result);

console.log(result);
