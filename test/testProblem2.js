//Problem 2 : Information about last car of inventory

const inventory = require("../inventory");
const problem2 = require("../problem2");

const result = problem2(inventory);

console.log("Last car is a "+result.car_make+" "+result.car_model+".");
